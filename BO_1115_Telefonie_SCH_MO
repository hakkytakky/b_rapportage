--CHG0046439 Naam: Ömer Kocyigit

/*
********************************** Versiebeheer *******************************************************************************
Datum        	                Jira-nr        	            Naam                            Opmerkingen
??-??-????                  v1.0                        Eric Hakvoort	                    Intiële versie
??-??-????                  v1.1                        Martin van der Hoek			        Diverse aanpassingen
30-01-2018                  v1.2                        Martin van der Hoek			        Aanpassing agv BO-1330 Bespreekdesk_SNS onder BLG Midoffice ipv onder SNS Midoffice
10-12-2020		            BO-4511				        Ömer Kocyigit                       Toevoeging van ReB_Acceptatie_Premium & ReB_Acceptatie_Flex aan avycstat.COMPETENTIE_TKT, omzetten B_DWHRV naar DM_STER
*******************************************************************************************************************************
rapporteigenaren: Robin Hoogen
*/

REPLACE VIEW b_rapportage.BO_1115_Telefonie_SCH_MO AS

SELECT CASE WHEN ddat2.naam_weekdag = 'Maandag' THEN CURRENT_DATE - 1
       WHEN ddat2.naam_weekdag = 'Dinsdag' THEN CURRENT_DATE - 2
       WHEN ddat2.naam_weekdag = 'Woensdag' THEN CURRENT_DATE - 3
       WHEN ddat2.naam_weekdag = 'Donderdag' THEN CURRENT_DATE - 4
       WHEN ddat2.naam_weekdag = 'Vrijdag' THEN CURRENT_DATE - 5
       WHEN ddat2.naam_weekdag = 'Zaterdag' THEN CURRENT_DATE - 6
	   ELSE CURRENT_DATE
	   END AS Peildatum
, ddat.jaar_vd_kalender		
, ddat.maand_vh_jaar
, ddat.week_vh_jaar			
, ddat.jaar_vd_kalender*100+ddat.week_vh_jaar AS Periode			
, avycstat.STATISTIEK_DT			
, avycstat.applicatie_tkt			
, avycstat.COMPETENTIE_TKT		
,CASE WHEN avycstat.applicatie_tkt IN ('ReB_ISD', 'ReB_Klantcontact') THEN 'ReB Midoffice' -- aangepast in BO-4511
 WHEN avycstat.applicatie_tkt = 'BLG_Midoffice' THEN 'BLG Midoffice'
 WHEN avycstat.applicatie_tkt = 'S_MO' AND avycstat.COMPETENTIE_TKT = 'S_MO_FC' THEN 'FinanCenter'
-- Volgende regel toegevoegd agv BO-1330 
 WHEN avycstat.applicatie_tkt = 'S_MO' AND avycstat.COMPETENTIE_TKT = 'BLG_Bespreekdesk_SNS' THEN 'BLG Midoffice'
-- Extra code als workaround om AVAYA incident te omzeilen START
WHEN avycstat.applicatie_tkt = 'Master_script' AND avycstat.COMPETENTIE_TKT IN ('ReB_Acceptatie_Flex', 'ReB_Acceptatie_Premium', 'Reb_Acceptatie_T1','ReB_Acceptatie_T2','ReB_Acceptatie_T3') THEN 'ReB Midoffice' --aangepast in BO-4511
-- Volgende regel aangepast agv BO-1330 
 WHEN avycstat.applicatie_tkt = 'Master_script' AND avycstat.COMPETENTIE_TKT IN ('BLG_Bespreekdesk','BLG_Bespreekdesk_SNS','BLG_MO_Overloop_Niet_Herkent','BLG_MO_Team_1','BLG_MO_Team_1_Noord','BLG_MO_Team_1_Zuid','BLG_MO_Team_2','BLG_MO_Team_2_Noord','BLG_MO_Team_2_Zuid','BLG_MO_Team_3') THEN 'BLG Midoffice'
 WHEN avycstat.applicatie_tkt = 'Master_script' AND avycstat.COMPETENTIE_TKT IN ('S_MO_BenO','S_MO_SNS_Oost','S_MO_SNS_West','S_MO_SNS_Zuid','S_MO_SNS') THEN 'SNS Midoffice'
 WHEN avycstat.applicatie_tkt = 'Master_script' AND avycstat.COMPETENTIE_TKT IN ('S_MO_FC') THEN 'FinanCenter'
-- Extra code als workaround om AVAYA incident te omzeilen EINDE
  ELSE 'SNS Midoffice'
 END AS Groep2	
 
, SUM(avycstat.AANBOD_AANT) AS Aanbod			
, SUM(avycstat.BEANTWOORD_AANT) AS Beantwoord_Totaal			
, SUM(avycstat.BEANTWOORD_N_DREMPEL_SEC_AANT) AS Beantwoord_Na_SLA			
, SUM(avycstat.BEANTWOORD_AANT-avycstat.BEANTWOORD_N_DREMPEL_SEC_AANT)  AS Beantwoord_SLA			
, SUM(avycstat.OPGEHANGEN_V_DREMPEL_SEC_AANT) AS Ophangen_Voor			
, SUM(avycstat.OPGEHANGEN_N_DREMPEL_SEC_AANT) AS Ophangen_Na			
, SUM(avycstat.AANBOD_AANT-avycstat.BEANTWOORD_AANT-avycstat.OPGEHANGEN_N_DREMPEL_SEC_AANT-avycstat.OPGEHANGEN_V_DREMPEL_SEC_AANT) AS Aanbod_Antwoord_Ophangen			
, SUM(avycstat.BEANTWOORD_AANT+avycstat.OPGEHANGEN_N_DREMPEL_SEC_AANT+avycstat.OPGEHANGEN_V_DREMPEL_SEC_AANT) AS Gecorrigeerd_Aanbod			
, SUM(avycstat.BEANTWOORD_AANT+avycstat.OPGEHANGEN_N_DREMPEL_SEC_AANT) AS Gecorrigeerd_Aanbod_excl_voor_SLA_drempel
		/*,	
		SERVICELEVEL_PCT, LANGSTE_WACHTTIJD_SEC_AANT, GEMIDDELDE_WACHTTIJD_SEC_AANT,	
		GEMIDDELDE_OPHANGTIJD_SEC_AANT, GESPREKSTIJD_SEC_AANT, RCW_SERVICELEVEL_PCT,	
		BEGIN_DT, EIND_DT, GELDIG_IND, DWH_INSERT_DTS, DWH_TASK_ID*/	
FROM DWH.AVY_APPLICATIECOMPETENTIE_STAT avycstat			
			
JOIN DM_STER.dim_datum ddat			
ON ddat.dim_datum_id = avycstat.STATISTIEK_DT			
			
JOIN DM_STER.dim_datum ddat2
ON ddat2.dim_datum_id = CURRENT_DATE
			
WHERE avycstat.eind_dt = '9999-12-31'			
AND avycstat.GELDIG_IND = 1			
AND (			
		(avycstat.applicatie_tkt = 'ReB_ISD'	AND avycstat.COMPETENTIE_TKT LIKE 'ReB_Acceptatie%')
		OR	
        (avycstat.applicatie_tkt = 'ReB_klantcontact'	AND avycstat.COMPETENTIE_TKT LIKE 'ReB_Acceptatie%') -- toegevoegd in BO-4511	
        OR
		(avycstat.applicatie_tkt = 'BLG_Midoffice'	AND avycstat.COMPETENTIE_TKT <> 'Agent Queue To')
		OR	
		(avycstat.applicatie_tkt = 'S_MO')
		OR
		(avycstat.COMPETENTIE_TKT = 'BLG_Bespreekdesk_ReB')
-- Extra code als workaround om AVAYA incident te omzeilen START
		OR (avycstat.applicatie_tkt = 'Master_script' AND avycstat.COMPETENTIE_TKT IN 
										('BLG_Bespreekdesk'   -- Volgende regel toegevoegd agv BO-1330 
										,'BLG_Bespreekdesk_SNS'
										,'BLG_MO_Overloop_Niet_Herkent'
										,'BLG_MO_Team_1'
										,'BLG_MO_Team_1_Noord'
										,'BLG_MO_Team_1_Zuid'
										,'BLG_MO_Team_2'
										,'BLG_MO_Team_2_Noord'
										,'BLG_MO_Team_2_Zuid'
										,'BLG_MO_Team_3'
                                        ,'ReB_Acceptatie_Flex'              --toegevoegd in BO-4511
                                        ,'ReB_Acceptatie_Premium'       --toegevoegd in BO-4511
										,'Reb_Acceptatie_T1'
										,'ReB_Acceptatie_T2'
										,'ReB_Acceptatie_T3'
										,'S_MO_BenO'
										,'S_MO_FC'
										,'S_MO_SNS_Oost'
										,'S_MO_SNS_West'
										,'S_MO_SNS_Zuid'
										,'BLG_Bespreekdesk_ReB'
										))
-- Extra code als workaround om AVAYA incident te omzeilen EINDE
	)		
AND (avycstat.STATISTIEK_DT BETWEEN '2018-01-01' AND Peildatum OR avycstat.STATISTIEK_DT BETWEEN Peildatum -70 AND Peildatum)
AND (avycstat.AANBOD_AANT <> 0 OR avycstat.BEANTWOORD_AANT <> 0 OR avycstat.OPGEHANGEN_N_DREMPEL_SEC_AANT <> 0 OR avycstat.OPGEHANGEN_V_DREMPEL_SEC_AANT <> 0)			
GROUP BY 1,2,3,4,5,6,7,8,9;