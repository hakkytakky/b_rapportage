--CHG0053785 Naam:Lydwien Batterink

/*
********************************** Versiebeheer *******************************************************************************
Datum                     Jira-nr          Naam                	Opmerkingen
03-12-2019				BO-3496			   Anoir Mousslih	    Extra velden toegevoegd.	
06-09-2021              BO-5120      	   Stefan v.d. Laar     hyp_aanvraag_bronsysteem toegevoegd t.b.v. FORCE
10-01-2022				BO-5358			   Lydwien Batterink	Filter op Hysys toegevoegd aan cte_nhg
*******************************************************************************************************************************


Rapporteigenaren: Fred Dokter, Marc Knoben
Business kennishouder: Marc Knoben
*/



--REPLACE VIEW b_rapportage.ASK_fin_akkoord	AS

with cte_finan as 
(
SELECT oofl.offerte_oid
,max_hyp_nieuwe_hyp_inkomen_bg

FROM dwh.offerte_oordeel_l oofl

LEFT JOIN dwh.hys_financiering	finan
ON finan.oordeel_oid = oofl.oordeel_oid
AND finan.geldig_ind=1
AND finan.eind_dt = '9999-12-31'

WHERE oofl.eind_dt = '9999-12-31'
AND oofl.geldig_ind =1
QUALIFY ROW_NUMBER() OVER (PARTITION BY oofl.offerte_oid  ORDER BY oofl.oordeel_oid DESC)  = 1 --Laatste max_hyp pakken.. omdat een offerte meerdere Oordelen kan hebben, wil je de laatste hebben.
)

, cte_nhg as
(SELECT fad.dim_hyp_aanvraag_id
, MAX(nhg_ind) AS nhg_ind																

FROM DM_STER.feit_hyp_aanvraagdeel  fad																

JOIN DM_STER.dim_hyp_aanvraagdeel dad																
ON dad.dim_hyp_aanvraagdeel_id = fad.dim_hyp_aanvraagdeel_id 		

JOIN DM_STER.dim_datum dd
ON fad.dim_datum_id = (dd.dim_datum_id - dd.dag_vd_maand)

WHERE 	dd.dim_datum_id = DATE -1
AND     dad.bronsysteem = 'Hysys'

GROUP BY 1	
)


SELECT da.bronsysteem as hyp_aanvraag_bronsysteem -- Toegevoegd in BO-5120
, da.aanvraag_nr																	
,da.aanvraagversie_nr																	
, fa.dim_datum_id peil_dt	
,hypotheek_ind
,dkr.bank_naam																	
,nhg.NHG_IND																	
,EXTRACT (MONTH FROM laatste_fin_akkoord_dt) AS maand																	
,EXTRACT (YEAR FROM laatste_fin_akkoord_dt) AS jaar
,da.laatste_annulering_aanvraag_dt
,da.laatste_fin_akkoord_dt
,CASE 
          WHEN  (laatste_fin_akk_boven_chf_ind = 1 
		                 OR laatste_fin_akk_maatwerk_ind = 1 )																	
						 AND laatste_fin_akk_overrule_reden NOT IN ( 'Overschrijding Maatschappij normen; past binnen GHF' )																	
						 AND laatste_fin_akk_overrule_reden NOT LIKE ANY ( 'Standaard uitzondering LTI%' )																	
          THEN 1 
		  ELSE 0 
 end AS LTI_boven_GHF_nom																	
,laatste_fin_akk_overrule_ind																	
,laatste_fin_akk_overrule_reden																	
,laatste_fin_akk_ask_kleur																	
,laatste_fin_akk_kleur																	
,CASE 
           WHEN nhg_ind	=1 
		        AND dkl.klant_heeft_hypotheek_ind = 1 
			THEN 'bestaand met NHG'																
			WHEN nhg_ind	=1 
			    AND (dkl.klant_heeft_hypotheek_ind = 0 
				OR dkl.klant_heeft_hypotheek_ind IS NULL) 
			THEN 'nieuw met NHG'														
			WHEN nhg_ind	=0 
			    AND dkl.klant_heeft_hypotheek_ind = 1 
			THEN 'bestaand zonder NHG'														
			WHEN nhg_ind	=0 
			    AND  (dkl.klant_heeft_hypotheek_ind = 0 
				OR dkl.klant_heeft_hypotheek_ind IS NULL) 
			THEN 'nieuw zonder NHG'														
 end AS type_klant	
,da.aanvraag_status	
,fa.hypotheek_bg
,fa.lening_bg
,dha.toetsinkomen_bg
,dha.geboorte_dt
,fa.marktwaarde_bg AS marktwaarde_bg
,fa.overbruggingskrediet_bg AS overbruggingskrediet_bg
,fa.bouwdepot_bg AS bouwdepot_bg
,da.laatste_fin_akk_lti_waarde --laatste ipv eerste
,da.ltv_waarde AS ltv_waarde
,da.aanvraagsoort AS aanvraagsoort
,da.aanvrager_aant AS aanvrager_aant
-- --eruit gehaald tot Rene besloten heeft hoe wij de dubbele rijen moeten verwerken.
/*	,hib.inkomstenbron_cd AS dienstverband
	, d.decode_omschr AS inkomstenbron
	,hib.ingang_Dt AS ingang_dienstverband
	,hib_m.inkomstenbron_cd AS dienstverband_m
	, de.decode_omschr AS inkomstenbron_m
	,hib_m.ingang_Dt AS ingang_dienstverband_m
*/	--
,hot.contr_met_achterstand_aant AS Aantal_BKR_achterstanden
,hot.geopende_contr_laatste_jr_aant AS Aantal_geopende_contracten_BKR_laatste_jaar
--,hop.kadaster_object_cd
,da.woningtype
,finan.max_hyp_nieuwe_hyp_inkomen_bg
,hot.SCORE_RESULTAAT_TKT  AS ASK_score
,CASE 
        WHEN dkl.klant_heeft_hypotheek_ind = 1 
		THEN 'bestaand'																
		WHEN dkl.klant_heeft_hypotheek_ind = 0 
		THEN 'nieuw'														
		ELSE 'Onbekend'
 END AS type_klant2
,SUM(CASE 
               WHEN hib.inkomstenbron_cd NOT IN ('08','21') 
			   THEN NULL 
			   ELSE CAST( hib.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2)) 
		  end) AS jaarink_Pensioen
,SUM(CASE 
               WHEN hib.inkomstenbron_cd NOT IN  ('01','03') 
			   THEN NULL 
			   ELSE CAST( hib.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2)) 
		  end) AS jaarink_Vast
,SUM(CASE 
               WHEN hib.inkomstenbron_cd NOT IN ('02','04') 
			   THEN NULL 
			   ELSE CAST( hib.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2))
		 end) AS jaarink_TijdmInt
,SUM(CASE 
               WHEN hib.inkomstenbron_cd NOT IN  ('14','15') 
			   THEN NULL 
			   ELSE CAST( hib.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2)) 
		  end) AS jaarink_TijdzInt
,SUM(CASE 
               WHEN hib.inkomstenbron_cd NOT IN ('05','13','19') 
			   THEN NULL 
			   ELSE CAST( hib.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2)) 
		  end) AS jaarink_Zelfst
,SUM(CASE 
               WHEN hib.inkomstenbron_cd NOT IN ('16') 
			   THEN NULL 
			   ELSE CAST( hib.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2))
		  end) AS jaarink_Flex
,SUM(CASE 
               WHEN hib.inkomstenbron_cd NOT IN ('07','10', '11', '12','20', '18') 
			   THEN NULL 
			   ELSE CAST( hib.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2)) 
		  end) AS jaarink_Overig
,SUM(CASE 
                WHEN hib.inkomstenbron_cd NOT IN  ('17') 
				THEN NULL 
				ELSE CAST( hib.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2)) 
		  end) AS jaarink_Seizoen
,SUM(CASE 
                WHEN hib.inkomstenbron_cd NOT IN  ('6','24', '25', '26', '27', '28') 
				THEN NULL 
				ELSE CAST( hib.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2)) 
				end) AS jaarink_Loonvaste_Ziekteuitkering
,SUM(CASE 
               WHEN hib_m.inkomstenbron_cd NOT IN ('08','21') 
			   THEN NULL 
			   ELSE CAST( hib_m.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2)) 
		  end) AS jaarink_Pensioen_m
,SUM(CASE 
               WHEN hib_m.inkomstenbron_cd NOT IN  ('01','03') 
			   THEN NULL 
			   ELSE CAST( hib_m.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2)) 
		  end) AS jaarink_Vast_m
,SUM(CASE 
               WHEN hib_m.inkomstenbron_cd NOT IN ('02','04') 
			   THEN NULL 
			   ELSE CAST( hib_m.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2)) 
		  end) AS jaarink_TijdmInt_m
,SUM(CASE 
                WHEN hib_m.inkomstenbron_cd NOT IN  ('14','15') 
				THEN NULL 
				ELSE CAST( hib_m.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2)) 
		  end) AS jaarink_TijdzInt_m
,SUM(CASE 
               WHEN hib_m.inkomstenbron_cd NOT IN ('05','13','19') 
			   THEN NULL 
			   ELSE CAST( hib_m.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2)) 
		  end) AS jaarink_Zelfst_m
,SUM(CASE 
               WHEN hib_m.inkomstenbron_cd NOT IN ('16') 
			   THEN NULL 
			   ELSE CAST( hib_m.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2)) 
		  end) AS jaarink_Flex_m
,SUM(CASE 
               WHEN hib_m.inkomstenbron_cd NOT IN ('07','10', '11', '12','20', '18') 
			   THEN NULL 
			   ELSE CAST( hib_m.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2)) 
		  end) AS jaarink_Overig_m
,SUM(CASE 
               WHEN hib_m.inkomstenbron_cd NOT IN  ('17') 
			   THEN NULL 
			   ELSE CAST( hib_m.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2)) 
		  end) AS jaarink_Seizoen_m
,SUM(CASE 
               WHEN hib_m.inkomstenbron_cd NOT IN  ('6','24', '25', '26', '27', '28') 
			   THEN NULL 
			   ELSE CAST( hib_m.BRUTO_JAARINKOMEN_BG AS DECIMAL (10,2)) 
		 end) AS jaarink_loonvaste_Ziekteuitkering_m
	
FROM DM_STER.dim_datum dd
	
JOIN DM_STER.feit_hyp_Aanvraag fa																	
ON fa.dim_datum_id = (dd.dim_datum_id - dd.dag_vd_maand)
																		
JOIN DM_STER.dim_hyp_aanvraag da																	
ON da.dim_hyp_aanvraag_id = fa.dim_hyp_Aanvraag_id																	
																		
JOIN DM_STER.dim_kantoor dkr																	
ON  dkr.dim_kantoor_id = fa.dim_kantoor_id		
	
LEFT JOIN dwh.hys_ols_toets hot
ON hot.offerte_oid = da.offerte_oid
AND hot.eind_dt = '9999-12-31'
AND hot.geldig_ind =1
	
/*	 -- Krijg bij een Offerte meerdere objecten terug.. object van bestaande woning en nieuwe woning.. ff uizoeken hoe, maar zit ook in ster, dus gebruik nu die.
LEFT JOIN dwh.object_offerte_l ool
	ON ool.offerte_oid = da.offerte_oid
	AND ool.geldig_ind = 1 
	AND ool.begin_dt <= dd.dim_datum_id
	AND ool.eind_dt > dd.dim_datum_id
	AND ool.object_offerte_rol_cd = 2
	
		LEFT JOIN dwh.hys_onderpand hop
	ON hop.object_oid = ool.object_oid
	AND hop.geldig_ind = 1
	AND hop.begin_dt <= dd.dim_datum_id
	AND hop.eind_dt > dd.dim_datum_id
	*/
	
LEFT JOIN cte_finan finan
ON finan.offerte_oid = da.offerte_oid
	
LEFT OUTER JOIN DM_STER.dim_hyp_aanvrager dha																	
ON dha.dim_hyp_Aanvrager_id  =  fa.dim_hyp_aanvrager_id																	
	
LEFT OUTER JOIN dwh.hys_inkomensbron hib
ON dha.partij_oid = hib.PARTIJ_OID
and hib.geldig_ind = 1
and hib.eind_dt = '9999-12-31'
	
LEFT OUTER JOIN DM_STER.dim_hyp_aanvrager dha_m																	
ON dha_m.dim_hyp_Aanvrager_id  =  fa.dim_hyp_aanvrager_id_mede
	
LEFT OUTER JOIN dwh.hys_inkomensbron hib_m
ON dha_m.partij_oid = hib_m.PARTIJ_OID	
and hib_m.geldig_ind = 1
and hib_m.eind_dt = '9999-12-31'
																
LEFT OUTER JOIN DM_STER.dim_klant dkl																	
ON dkl.sas_klant_nr = dha.klant_nr																	
AND dkl.dm_begin_dt <= da.eerste_offerte_dt
AND dkl.dm_eind_dt > da.eerste_offerte_dt		
AND dkl.bronsysteem = 'SAS'
																		
LEFT OUTER JOIN  cte_nhg nhg																
ON nhg.dim_hyp_aanvraag_id = fa.dim_hyp_aanvraag_id																
																		
WHERE dd.dim_datum_id = DATE -1
AND da.laatste_fin_akkoord_dt >= '2014-01-01' 																
AND da.omzetting_ind = 0		
AND (laatste_annulering_aanvraag_dt IS NULL OR laatste_annulering_aanvraag_dt < laatste_fin_akkoord_dt)
and upper(da.bronsysteem) = 'HYSYS'

GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35;