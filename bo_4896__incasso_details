-- CHG0048887 naam: Stefan van de Laar
/*
********************************** Versiebeheer *******************************************************************************
Datum                       Jira-nr                   Naam                            Opmerkingen
xx-xx-xxxx                 -                            -                                     - Initiele versie
14-05-2021              BO-4896               Stefan v.d. Laar             Aanpassen van rapportage zodat deze geautomatiseerd middels Tableau uitgeleverd kan worden
*******************************************************************************************************************************
Rapportage het aantal incasso welke utigevoerd en gestorneerd zijn (door klant en door de bank)

Rapporteigenaren:  Robert Bloos
Business kennishouder:  incasso@devolksbank.nl, Robert.Bloos
*/


replace view s_dm_operations.bo_4896__incasso_details as 

-- Datum selectie
with cte_datum as (
select dim_datum_id
, cast(datum_jjjjmmdd as decimal(8,0)) as datum_jjjjmmdd
, datum_jjjjmm
-- Onderstaande 2 bepalingingen om ervoor te zorgen dat de selecties van de afgelopen 14 afgesloten maanden mee te nemen
,trim(both ' ' from( extract(year from add_months(dim_datum_id,-14))))||trim(both ' ' from extract(month from (add_months(dim_datum_id,-14)))) as begin_periode_jjjjmm

, case 
         when extract(month from (add_months(dim_datum_id,-14)))>9 
		 then cast(trim(both ' ' from( extract(year from add_months(dim_datum_id,-14))))||trim(both ' ' from extract(month from (add_months(dim_datum_id,-14))))||'01' as decimal(8,0)) 
		 else cast(trim(both ' ' from( extract(year from add_months(dim_datum_id,-14))))||'0'||trim(both ' ' from extract(month from (add_months(dim_datum_id,-14))))||'01' as decimal(8,0)) 
end as begin_periode
from dm_Ster.dim_datum
where ultimo_maand_ind = 1
and dim_datum_id = CURRENT_DATE - EXTRACT(DAY FROM CURRENT_DATE) -- selectie om geautomatiseerd de laatst afgesloten maand te selecteren 
)

, cte_incasso as (
SELECT distinct -- de distinct is opgenomen om dubbele rijen te verwijderen. Deze dubbele rijen ontstaan doordat er per maand meerdere transacties per rekening gedaan kunnen worden.
Merk
,Klantnr
,Naam
,RSC
,Reknr
,Periode
,MIN(Boekings_bg) OVER (PARTITION BY Periode, Reknr) AS Min_bg_mnd
,MAX(Bedrag)  OVER (PARTITION BY Periode, Reknr) AS Max_bg_mnd
,AVG(Boekings_bg) OVER (PARTITION BY Periode, Reknr) AS Avg_bg_mnd
,SUM(Incasso_T)  OVER (PARTITION BY Periode, Reknr) AS Aantal_I_mnd
,SUM(Incasso_Bg) OVER (PARTITION BY Periode, Reknr) AS Totaal_I_mnd
,SUM(BankSt_T)  OVER (PARTITION BY Periode, Reknr) AS Aantal_B_mnd
,SUM(BankSt_Bg) OVER (PARTITION BY Periode, Reknr) AS Totaal_B_mnd
,SUM(KlantSt_T)  OVER (PARTITION BY Periode, Reknr) AS Aantal_K_mnd
,SUM(KlantSt_Bg) OVER (PARTITION BY Periode, Reknr) AS Totaal_K_mnd
,SUM(Batch_Bedrag)  OVER (PARTITION BY Periode, Reknr) AS Batch_B_mnd
,SUM(Batch_Aantal) OVER (PARTITION BY Periode, Reknr) AS Batch_Aantal_mnd
,MIN(Batch_Bedrag) OVER (PARTITION BY Periode, Reknr) AS Batch_Min_mnd
,MAX(Batch_Bedrag) OVER (PARTITION BY Periode, Reknr) AS Batch_Max_mnd
,SUM(KlantSt_T + BankSt_T) OVER (PARTITION BY Periode, Reknr) AS Aantal_St_mnd

-- Percentage bepaling van het aantal bank stornos
,CASE
          WHEN Aantal_I_mnd > 0 AND Aantal_B_mnd > 0 
		  THEN (CAST(Aantal_B_mnd AS DECIMAL(9,2))*100) /  CAST (Aantal_I_mnd AS DECIMAL(9,2))
		  ELSE 0 
  end AS BankStornoP_mnd
-- Percentage bepaling van het aantal klant stornos
,CASE 
             WHEN Aantal_I_mnd > 0 AND Aantal_K_mnd > 0 
			 THEN  (CAST(Aantal_K_mnd AS DECIMAL(9,2))*100) /  CAST (Aantal_I_mnd AS DECIMAL(9,2))
			 ELSE 0 
 end AS KlantStornoP_mnd
-- Percentage bepaling van het aantal stornos (klant+bank)
,CASE
           WHEN Aantal_I_mnd > 0 AND Aantal_B_mnd+Aantal_K_mnd> 0 
		   THEN  (CAST((Aantal_B_mnd+Aantal_K_mnd) AS DECIMAL(9,2))*100) /  CAST (Aantal_I_mnd AS DECIMAL(9,2))
		   ELSE 0 
 end AS StornoP_mnd
FROM
(
SELECT
sst.journaal_dt /100 AS Periode
,sst.REK_NR AS Reknr
--Batch uistluiten
,CASE 
        WHEN sst.Betaler_rek_nr <> 'BATCH' 
		THEN sst.overeenkomst_bg 
		ELSE 0
end AS Bedrag
-- Bankstorno
,CASE
        WHEN sst.RETOUR_REDEN_TKT IN ( 'MS03','AM04','AC04','MD07','AC01','AC06','AG01','RR04', 'AG02')  OR sst.sepa_trans IN (9715) 
		THEN sst.overeenkomst_bg
		ELSE 0
  end AS  BankSt_Bg
,CASE
      WHEN sst.RETOUR_REDEN_TKT IN ( 'MS03','AM04','AC04','MD07','AC01','AC06','AG01','RR04', 'AG02') OR sst.sepa_trans IN (9715) 
	  THEN 1
	  ELSE 0
  end AS  BankSt_T
--Klantstorno
,CASE
       WHEN sst.RETOUR_REDEN_TKT IN ('MD01','MD06', 'MS02', 'SL01') OR sst.sepa_trans IN (9716) 
	   THEN sst.overeenkomst_bg
	   ELSE 0
 end AS  KlantSt_Bg
,CASE
       WHEN sst.RETOUR_REDEN_TKT IN ('MD01','MD06', 'MS02', 'SL01') OR sst.sepa_trans IN (9716) 
	   THEN 1
	   ELSE 0
 end AS  KlantSt_T
--Incasso
,CASE
       WHEN sst.sepa_trans IN (8820,8821)  AND  sst.Betaler_rek_nr <> 'BATCH' 
	   THEN   sst.overeenkomst_bg
	   ELSE 0
 end AS Incasso_Bg
,CASE
       WHEN sst.sepa_trans IN (8820,8821) AND sst.Betaler_rek_nr <> 'BATCH'  
	   THEN   1
	   ELSE 0
 end AS Incasso_T
--Batch gegevens ophalen
,CASE
       WHEN  sst.Betaler_rek_nr = 'BATCH' 
	   THEN sst.Overeenkomst_bg
	   ELSE 0
 end AS Batch_Bedrag
,CASE
       WHEN  sst.Betaler_rek_nr= 'BATCH' 
	   THEN 1
	   ELSE 0
end AS Batch_Aantal
 
,sst.sepa_trans AS Tcode
,sst.overeenkomst_bg AS Boekings_bg
,crs.BANK_NR AS Merk
,sk.KLANT_NR AS Klantnr
,crs.BRON_CONTRACTSOORT_NR AS RSC
,sk.NAAM AS Naam

FROM cte_datum ddm

join dwh.sas_sepa_transactie sst
on sst.journaal_dt >=ddm.begin_periode
and sst.journaal_dt <= ddm.datum_jjjjmmdd
and sst.geldig_ind = 1
and sst.eind_dt = '9999-12-31' -- hier is bewust afgeweken van de standaard geldigheidsbelang zoals deze verder hieronder is toegepast. Transacties blijven in principe altijd geldig waardoor de geen echte einddatum wordt ingevuld
and sst.SEPA_TRANS IN (8820,8821,9715,9716,9717) 
/* 8820=DOORLOPENDE MACHTIGING ALGEMEEN
    8821=EENMALIGE MACHTIGING
	9715=SEPA DD INITIATION RETURN
	9716=SEPA DD INITIATION REFUND
	9717=SEPA DD INITIATION REVERSAL
*/

LEFT JOIN DWH.contract_s crs
ON sst.CONTRACT_OID = crs.CONTRACT_OID
AND crs.GELDIG_IND = 1
and crs.begin_dt <= ddm.dim_datum_id
and crs.eind_dt > ddm.dim_datum_id

LEFT JOIN DWH.contract_partij_l cpl
ON sst.CONTRACT_OID = cpl.CONTRACT_OID
AND cpl.GELDIG_IND = 1
and cpl.begin_dt <= ddm.dim_datum_id
AND cpl.EIND_DT > ddm.dim_datum_id
AND cpl.CONTRACT_PARTIJ_ROL_CD = 1
and cpl.relatie_begin_dt <= ddm.dim_datum_id
AND (cpl.RELATIE_EIND_DT  >=  CAST(CAST(sst.journaal_dt AS CHAR(8)) AS DATE FORMAT 'yyyymmdd')  OR cpl.Relatie_eind_dt IS NULL)

LEFT JOIN DWH.sas_klant sk
ON sk.PARTIJ_OID = cpl.PARTIJ_OID
AND sk.GELDIG_IND = 1
and sk.begin_dt <= ddm.dim_datum_id
AND sk.EIND_DT > ddm.dim_datum_id
 )sub
 )


select
 ddm.dim_datum_id as peil_dt
 ,Merk
, Klantnr
, Naam
, RSC
, Reknr
, Periode
, max(Min_bg_mnd) as Min_bg_mnd
, max(Max_bg_mnd) as Max_bg_mnd
, max(Avg_bg_mnd) as Avg_bg_mnd
, max(Aantal_I_mnd) as Aantal_I_mnd
, max(Totaal_I_mnd) as Totaal_I_mnd
, max(Aantal_B_mnd) as Aantal_B_mnd
, max(Totaal_B_mnd) as Totaal_B_mnd
, max(Aantal_K_mnd) as Aantal_K_mnd
, max(Totaal_K_mnd) as Totaal_K_mnd
, max(Batch_B_mnd) as Batch_B_mnd
, max(Batch_Aantal_mnd) as Batch_Aantal_mnd
, max(Batch_Min_mnd) as Batch_Min_mnd
, max(Batch_Max_mnd) as Batch_Max_mnd
, max(Aantal_St_mnd) as Aantal_St_mnd
, max(BankStornoP_mnd) as BankStornoP_mnd
, max(KlantStornoP_mnd) as KlantStornoP_mnd
, max(StornoP_mnd) as StornoP_mnd
from (
select 
 Merk
, Klantnr
, Naam
, RSC
, Reknr
, Periode
, Min_bg_mnd
, Max_bg_mnd
, Avg_bg_mnd
, Aantal_I_mnd
, Totaal_I_mnd
, Aantal_B_mnd
, Totaal_B_mnd
, Aantal_K_mnd
, Totaal_K_mnd
, Batch_B_mnd
, Batch_Aantal_mnd
, Batch_Min_mnd
, Batch_Max_mnd
, Aantal_St_mnd
, BankStornoP_mnd
, KlantStornoP_mnd
, StornoP_mnd
from cte_incasso
union all
-- onderstaande selectie is bedoeld om voor de rekeningen, welke in de periode een incasso hebben gehad, de mogelijk ontbrekende maanden aan te vullen met 0 regels
SELECT
i.Merk
,i.Klantnr
,i.Naam
,i.RSC
,i.Reknr
,ddm.datum_jjjjmm_int AS Periode
,0 AS Min_bg_mnd
,0 AS Max_bg_mnd
,0 AS Avg_bg_mnd
,0 AS Aantal_I_mnd
,0 AS Totaal_I_mnd
,0 AS Aantal_B_mnd
,0 AS Totaal_B_mnd
,0 AS Aantal_K_mnd
,0 AS Totaal_K_mnd
,0 AS Batch_B_mnd
,0 AS Batch_Aantal_mnd
,0 AS Batch_Min_mnd
,0 AS Batch_Max_mnd
,0 AS Aantal_St_mnd
,0 AS BankStornoP_mnd
,0 AS KlantStornoP_mnd
,0 AS StornoP_mnd
FROM cte_incasso i
CROSS JOIN dm_ster.dim_datum ddm
where ddm.ultimo_maand_ind = 1
and ddm.offset_tov_huidige_maand_nr >= -15 -- De selectie is bedoeld om de laatste 15 maanden mee te nemen
and ddm.offset_tov_huidige_maand_nr < 0
) sub
join cte_datum ddm
on sub.Periode BETWEEN ddm.begin_periode_jjjjmm AND ddm.datum_jjjjmm
group by 1,2,3,4,5,6,7;