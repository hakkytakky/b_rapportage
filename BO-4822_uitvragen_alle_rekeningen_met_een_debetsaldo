/*
********************************** Versiebeheer *******************************************************************************
Datum                       Jira-nr                   Naam                            Opmerkingen
16-04-2021               BO-4822                Stefan v.d. Laar             - Initiele versie
06-05-2021              BO-4872                 Ömer Kocyigit                  Opsplitsen kolommen actieve_rekeningen en tegenrekeningen in meerdere kolommen en toevoegen saldo_bg aan deze kolommen
*******************************************************************************************************************************
Rapportage over beleggings- en spaarrekeningen welke een debetstand hebben

Rapporteigenaren:  George de Graaf
Business kennishouder: George de Graaf
*/

--REPLACE VIEW b_rapportage.BO_4822_belegging_en_spaarrek_met_debetstand AS 

WITH cte_eb_contract AS (
SELECT dim_datum_id
, contract_oid
, MAX(CASE WHEN rijnummer = 1 THEN TRIM (TRAILING '.' FROM (eb_contract_nr))||TRIM (TRAILING '.' FROM ('--')) ELSE '' END) AS eb_contract_nr_1
,MAX(CASE WHEN rijnummer = 2 THEN TRIM (TRAILING '.' FROM (eb_contract_nr))||TRIM (TRAILING '.' FROM ('--')) ELSE '' END) AS eb_contract_nr_2
,MAX(CASE WHEN rijnummer = 3 THEN TRIM (TRAILING '.' FROM (eb_contract_nr))||TRIM (TRAILING '.' FROM ('--')) ELSE '' END) AS eb_contract_nr_3
FROM (
SELECT ddm.dim_datum_id
, ccl.gerelateerd_contract_oid AS contract_oid
, sate.eb_contract_nr
, ROW_NUMBER()OVER(PARTITION BY ccl.gerelateerd_contract_oid ORDER BY sate.contract_oid) AS rijnummer
FROM dm_ster.dim_datum ddm

JOIN dwh.contract_contract_l ccl
ON ccl.begin_dt <= ddm.dim_datum_id
AND ccl.eind_dt > ddm.dim_datum_id
AND ccl.geldig_ind = 1
AND ccl.relatie_begin_dt <= ddm.dim_datum_id
AND (ccl.relatie_eind_dt > ddm.dim_datum_id OR ccl.relatie_eind_dt IS NULL)
AND ccl.contract_rol_cd = 300

JOIN dwh.sas_admin_token_uitgifte sate
ON sate.contract_oid= ccl.relateert_aan_contract_oid
AND sate.geldig_ind = 1
AND sate.begin_dt <= ddm.dim_datum_id
AND sate.eind_dt > ddm.dim_datum_id
AND sate.effectief_dt <= ddm.datum_jjjjmmdd
AND sate.afloop_dt > ddm.datum_jjjjmmdd
AND sate.token_status_cd = 40 --  40= Aktief

WHERE ddm.dim_datum_id = CURRENT_DATE-1
) sub
GROUP BY 1,2
) 
, cte_betaalspaar AS (
SELECT dim_datum_id
, bank_nr
, partij_oid
,MAX(CASE WHEN rijnummer = 1 THEN rekening_nr ELSE NULL END) AS rekening_nr_1
,MAX(CASE WHEN rijnummer = 2 THEN rekening_nr ELSE NULL END) AS rekening_nr_2
,MAX(CASE WHEN rijnummer = 3 THEN rekening_nr ELSE NULL END) AS rekening_nr_3
,MAX(CASE WHEN rijnummer = 4 THEN rekening_nr ELSE NULL END) AS rekening_nr_4
,MAX(CASE WHEN rijnummer = 5 THEN rekening_nr ELSE NULL END) AS rekening_nr_5
,MAX(CASE WHEN rijnummer = 1 THEN saldo_bg ELSE NULL END) AS saldo_bg_nr_1
,MAX(CASE WHEN rijnummer = 2 THEN saldo_bg ELSE NULL END) AS saldo_bg_nr_2
,MAX(CASE WHEN rijnummer = 3 THEN saldo_bg ELSE NULL END) AS saldo_bg_nr_3
,MAX(CASE WHEN rijnummer = 4 THEN saldo_bg ELSE NULL END) AS saldo_bg_nr_4
,MAX(CASE WHEN rijnummer = 5 THEN saldo_bg ELSE NULL END) AS saldo_bg_nr_5
FROM (
SELECT ddm.dim_datum_id
, dkr.bank_nr
, dkt.partij_oid
, drg.iban_nr AS rekening_nr
, frg.saldo_bg
, ROW_NUMBER()OVER(PARTITION BY dkt.partij_oid ORDER BY frg.contract_oid) AS rijnummer
FROM dm_ster.feit_rekening frg

JOIN dm_ster.dim_datum ddm
ON ddm.dim_datum_id = frg.dim_datum_id
AND ddm.dim_datum_id = CURRENT_DATE-1

JOIN dm_ster.dim_rekening drg
ON drg.dim_rekening_id = frg.dim_rekening_id
AND drg.afbetaald_ind = 0
AND (drg.afbetaald_dt IS NULL OR drg.afbetaald_dt >ddm.dim_datum_id)

JOIN dm_ster.dim_klant dkt
ON dkt.dim_klant_id = frg.dim_klant_id

JOIN dm_ster.dim_kantoor dkr
ON dkr.dim_kantoor_id = frg.dim_kantoor_id

JOIN dm_ster.dim_product dpt
ON dpt.dim_product_id = frg.dim_product_id
AND dpt.hoofdproductgroep_nr IN (1,2) --1= Betalen 2=sparen
AND dpt.productcategorie_nr = 1
) sub
GROUP BY 1,2,3
)

, cte_klantblok AS 
(SELECT dim_datum_id
, partij_oid
,MAX(CASE WHEN rijnummer = 1 THEN TRIM (TRAILING '.' FROM (blokkeringsreden_cd))||TRIM (TRAILING '.' FROM ('--')) ELSE '' END) AS klant_blok_1
,MAX(CASE WHEN rijnummer = 2 THEN TRIM (TRAILING '.' FROM (blokkeringsreden_cd))||TRIM (TRAILING '.' FROM ('--')) ELSE '' END) AS klant_blok_2
,MAX(CASE WHEN rijnummer = 3 THEN TRIM (TRAILING '.' FROM (blokkeringsreden_cd))||TRIM (TRAILING '.' FROM ('--')) ELSE '' END) AS klant_blok_3
,MAX(CASE WHEN rijnummer = 4 THEN TRIM (TRAILING '.' FROM (blokkeringsreden_cd))||TRIM (TRAILING '.' FROM ('--')) ELSE '' END) AS klant_blok_4
,MAX(CASE WHEN rijnummer = 5 THEN TRIM (TRAILING '.' FROM (blokkeringsreden_cd))||TRIM (TRAILING '.' FROM ('--')) ELSE '' END) AS klant_blok_5
FROM (
SELECT ddm.dim_datum_id
, skb.partij_oid
, skb.blokkeringsreden_cd
, ROW_NUMBER()OVER(PARTITION BY partij_oid ORDER BY blokkeringsreden_cd) AS rijnummer
FROM dm_Ster.dim_datum ddm

JOIN dwh.sas_klantblokkering skb
ON skb.begin_dt <= ddm.dim_datum_id
AND skb.eind_dt >  ddm.dim_datum_id
AND skb.effectief_dt <= ddm.datum_jjjjmmdd
AND skb.afloop_dt > ddm.datum_jjjjmmdd
AND skb.geldig_ind = 1

WHERE ddm.dim_datum_id = CURRENT_DATE-1
) sub
GROUP BY 1,2
)

, cte_reklok AS 
(SELECT dim_datum_id
,contract_oid
,MAX(CASE WHEN rijnummer = 1 THEN TRIM (TRAILING '.' FROM (blokkeringsreden_cd))||TRIM (TRAILING '.' FROM ('--')) ELSE '' END) AS rek_blok_1
,MAX(CASE WHEN rijnummer = 2 THEN TRIM (TRAILING '.' FROM (blokkeringsreden_cd))||TRIM (TRAILING '.' FROM ('--')) ELSE '' END) AS rek_blok_2
,MAX(CASE WHEN rijnummer = 3 THEN TRIM (TRAILING '.' FROM (blokkeringsreden_cd))||TRIM (TRAILING '.' FROM ('--')) ELSE '' END) AS rek_blok_3
,MAX(CASE WHEN rijnummer = 4 THEN TRIM (TRAILING '.' FROM (blokkeringsreden_cd))||TRIM (TRAILING '.' FROM ('--')) ELSE '' END) AS rek_blok_4
,MAX(CASE WHEN rijnummer = 5 THEN TRIM (TRAILING '.' FROM (blokkeringsreden_cd))||TRIM (TRAILING '.' FROM ('--')) ELSE '' END) AS rek_blok_5
FROM (
SELECT ddm.dim_datum_id
, srb.contract_oid
, srb.blokkeringsreden_cd
, ROW_NUMBER()OVER(PARTITION BY contract_oid ORDER BY blokkeringsreden_cd) AS rijnummer
FROM dm_Ster.dim_datum ddm

JOIN dwh.sas_rekening_blokkering srb
ON srb.begin_dt <= ddm.dim_datum_id
AND srb.eind_dt >  ddm.dim_datum_id
AND srb.effectief_dt <= ddm.datum_jjjjmmdd
AND srb.afloop_dt > ddm.datum_jjjjmmdd
AND srb.geldig_ind = 1

WHERE ddm.dim_datum_id = CURRENT_DATE-1
)sub
GROUP BY 1,2
)

, cte_tegenrek AS (
SELECT dim_datum_id
,contract_oid
,MAX(CASE WHEN rijnummer = 1 THEN rekening_nr_samen ELSE NULL END) AS tegenrekening_1
,MAX(CASE WHEN rijnummer = 2 THEN rekening_nr_samen ELSE NULL END) AS tegenrekening_2
,MAX(CASE WHEN rijnummer = 3 THEN rekening_nr_samen ELSE NULL END) AS tegenrekening_3
,MAX(CASE WHEN rijnummer = 1 THEN SALDO_BG ELSE NULL END) AS saldo_tegenrekening_1
,MAX(CASE WHEN rijnummer = 2 THEN SALDO_BG ELSE NULL END) AS saldo_tegenrekening_2
,MAX(CASE WHEN rijnummer = 3 THEN SALDO_BG ELSE NULL END) AS saldo_tegenrekening_3

FROM (
SELECT ddm.dim_datum_id
, ccl.relateert_aan_contract_oid AS contract_oid
, css.SALDO_BG
, COALESCE(stg.externe_iban_nr, sr.iban_nr) AS rekening_nr_samen
, ROW_NUMBER()OVER(PARTITION BY ccl.relateert_aan_contract_oid ORDER BY rekening_nr_samen) AS rijnummer

FROM dm_ster.dim_datum ddm

JOIN dwh.contract_contract_l ccl
ON ccl.begin_dt <= ddm.dim_datum_id
AND ccl.eind_dt >  ddm.dim_datum_id
AND ccl.geldig_ind = 1
AND ccl.relatie_begin_dt<= ddm.dim_datum_id
AND (ccl.relatie_eind_dt >  ddm.dim_datum_id OR ccl.relatie_eind_dt IS NULL)
AND ccl.contract_rol_cd IN (3,4,401,402) --3 = saldo overboeken naar, 4= incasseren van, 401= extern overboeken naar, 402= extern incasseren van

LEFT JOIN dwh.sas_Tegenrekening stg
ON stg.extern_contract_oid = ccl.gerelateerd_contract_oid
AND stg.begin_dt <= ddm.dim_datum_id
AND stg.eind_dt > ddm.dim_datum_id

LEFT JOIN dwh.sas_rekening_lf sr 
ON sr.CONTRACT_OID = ccl.GERELATEERD_CONTRACT_OID
AND sr.geldig_ind = 1
AND sr.begin_dt <= ddm.dim_datum_id
AND sr.eind_dt > ddm.dim_datum_id

LEFT JOIN dwh.contract_saldo_s css
ON css.contract_oid = sr.contract_oid
AND css.geldig_ind = 1
AND css.begin_dt <= DATE-1
AND css.eind_dt = '9999-12-31'
AND css.SALDO_START_DT <= DATE-1
AND css.SALDO_EIND_DT = '9999-12-31'

WHERE ddm.dim_datum_id = CURRENT_DATE-1
) sub
GROUP BY 1,2
)

SELECT ddm.dim_datum_id AS peil_dt
,dkr.bank_naam AS merk
, dpt.product_nr AS rsc
, dpt.product_naam
, dkt.klant_nr
, drg.iban_nr AS rekening_nr
, frg.saldo_bg
, drg.eerste_maal_debet_dt AS eerste_datum_roodstand
, drg.eerste_overtrekking_dt
, srf.laatste_mutatie_dt AS laatste_mutatie_dt
, bsr.rekening_nr_1
,bsr.saldo_bg_nr_1
, bsr.rekening_nr_2 
,bsr.saldo_bg_nr_2
, bsr.rekening_nr_3
,bsr.saldo_bg_nr_3
, bsr.rekening_nr_4
,bsr.saldo_bg_nr_4
, bsr.rekening_nr_5
,bsr.saldo_bg_nr_5
, tegenrek.tegenrekening_1
,saldo_tegenrekening_1
,tegenrek.tegenrekening_2
,saldo_tegenrekening_2
,tegenrek.tegenrekening_3 
,saldo_tegenrekening_3
, rekblk.rek_blok_1||rekblk.rek_blok_2||rekblk.rek_blok_3||rekblk.rek_blok_4|| rekblk.rek_blok_5 AS rekening_blokkeringen
, klntblk.klant_blok_1||klntblk.klant_blok_2|| klntblk.klant_blok_3||klntblk.klant_blok_4||klntblk.klant_blok_5 AS klant_blokkeringen
, eb.eb_contract_nr_1||eb.eb_contract_nr_2||eb.eb_contract_nr_3 AS eb_contracten
, dce.email_adres

FROM dm_ster.feit_rekening frg

JOIN dm_ster.dim_datum ddm
ON ddm.dim_datum_id = frg.dim_datum_id
AND ddm.dim_datum_id = CURRENT_DATE-1

JOIN dm_ster.dim_rekening drg
ON drg.dim_rekening_id = frg.dim_rekening_id

JOIN dm_ster.dim_klant dkt
ON dkt.dim_klant_id = frg.dim_klant_id

JOIN dm_ster.dim_kantoor dkr
ON dkr.dim_kantoor_id = frg.dim_kantoor_id

JOIN dm_ster.dim_product dpt
ON dpt.dim_product_id = frg.dim_product_id
AND dpt.hoofdproductgroep_nr IN (2,3) --2=sparen 3=beleggen
AND dpt.productcategorie_nr = 1

LEFT JOIN dm_ster.dim_correspondentie dce
ON dce.partij_oid= dkt.partij_oid
AND dce.dm_begin_dt <= ddm.dim_datum_id
AND dce.dm_eind_dt > ddm.dim_datum_id

LEFT JOIN dwh.sas_rekening_hf srf
ON srf.contract_oid = frg.contract_oid
AND srf.geldig_ind = 1
AND srf.begin_dt <= ddm.dim_datum_id
AND srf.eind_dt >ddm.dim_datum_id

LEFT JOIN cte_eb_contract eb
ON eb.contract_oid = frg.contract_oid
AND eb.dim_datum_id = ddm.dim_datum_id

LEFT JOIN cte_betaalspaar bsr
ON bsr.partij_oid = dkt.partij_oid
AND bsr.dim_datum_id = ddm.dim_datum_id
AND bsr.bank_nr = dkr.bank_nr

LEFT JOIN cte_klantblok klntblk
ON klntblk.partij_oid = dkt.partij_oid
AND klntblk.dim_datum_id = ddm.dim_datum_id

LEFT JOIN cte_reklok rekblk
ON rekblk.contract_oid = frg.contract_oid
AND rekblk.dim_datum_id = ddm.dim_datum_id

LEFT JOIN cte_tegenrek tegenrek
ON tegenrek.contract_oid = frg.contract_oid
AND tegenrek.dim_datum_id = ddm.dim_datum_id

WHERE frg.debetsaldo_bg > 0;
