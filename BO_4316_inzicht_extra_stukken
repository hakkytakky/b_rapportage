
/*
********************************** Versiebeheer *******************************************************************************
Datum        	    Jira-nr        	    Naam                   Opmerkingen
25-09-2020 	 	BO-4316       	Ömer Kocyigit       Intiële versie
21-09-2021      BO-5130         Stefan v.d. Laar    - view hernoemd naar _old. De view is niet meer relevant voor gebruik in Tableau
                                                    - View opgenomen in Bitbucket voor archief doeleinden   
*******************************************************************************************************************************
Beschrijving: Code voor inzicht in extra opgevraagde stukken na het uitbrengen van het rentevoorstel
Label: Service Center Hypotheken
rapporteigenaren: Erik Bulk
*/

REPLACE VIEW b_rapportage.BO_4316_inzicht_extra_stukken AS 

WITH CTE_FTR AS

	( SELECT  fa.dim_datum_id AS kalender_datum
       		, hbs.offerte_oid
       		, dkr.bank_naam
       		, da.aanvraag_nr
       		, da.aanvraag_status
       		, da.omzetting_ind
       		, CASE WHEN (dav.inkomsten_uit_zelfstandig_bg >0  
                     OR dav.zelfstandig_inkomen_ind = 1) 
                     OR  (davm.inkomsten_uit_zelfstandig_bg >0   -- davm. haalt data mede-aanvrager op
                     OR davm.zelfstandig_inkomen_ind = 1) 
              	   	THEN 1 
              	   ELSE 0 
              END AS zakelijk_inkomen_ind

			, CASE WHEN din.dim_intermediair_id <> -2 THEN din.bedieningsconcept 
                   ELSE 'nvt' 
              END AS bedieningsconcept
			  
       		, da.invoer_aanvraag_dt
    
       		, COALESCE (hbs.NIET_AKKOORD_DT,hbs.AKKOORD_DT ,hbs.NIET_NODIG_DT,hbs.ONTVANGST_DT,hbs.begin_dt) AS BEGIN_DT
       		, hbs.BEWIJSSTUK_INSTANCEID
       		, hbs.BEWIJSSTUK_NR
       		, hbs.BEWIJSSTUK_TYPE_TKT
       		, hbs.BEWIJSSTUK_CATEGORIE_CD
 
       		, hbs.INTERNE_STATUS_CD_TKT
       		, hbs.BEWIJSSTUK_STATUS_CD
       		, hbs.BEWIJSSTUK_OMSCHRIJVING_TKT

			, aanv_deel.nhg_ind
			, CASE WHEN haa.betreft_maatwerk_tkt = 'true' THEN 1
				   WHEN haa.betreft_maatwerk_tkt = 'false' THEN 0 
				ELSE NULL 
			  END AS betreft_maatwerk_tkt
			, hbs.wijziging_dt
       FROM DM_STER.feit_hyp_aanvraag fa
	   
       JOIN DM_STER.dim_hyp_aanvraag da            
       ON fa.dim_hyp_aanvraag_id = da.dim_hyp_aanvraag_id
       AND fa.dim_datum_id = (
	   						  SELECT kalender_datum 
							  FROM b_rapportage.kalender_datum 
							 )
       
       JOIN DM_STER.dim_hyp_aanvrager dav           
       ON dav.dim_hyp_aanvrager_id = fa.dim_hyp_aanvrager_id
       
       JOIN DM_STER.dim_hyp_aanvrager davm   
       ON davm.dim_hyp_aanvrager_id = fa.dim_hyp_aanvrager_id_mede
       
	   JOIN DM_ster.dim_kantoor dkr
	   ON dkr.dim_kantoor_id = fa.dim_kantoor_id

       JOIN DM_STER.dim_intermediair din           
       ON din.dim_intermediair_id = fa.dim_intermediair_id
	   
	   LEFT OUTER JOIN dwh.hys_aanvraag haa
			ON haa.offerte_oid = da.offerte_oid
			AND haa.geldig_ind = 1
		    AND haa.begin_dt <= DATE-1 
            AND haa.eind_dt > DATE-1 
       
	   LEFT JOIN 
	   		( /*Bepaling NHG indicator zit in aanvraagdeel, om te dubbelingen te voorkomen (meerdere aanvraagdelen per hypotheek) slaan we hem hier plat*/
			  SELECT aanvraag_nr
			  		,MAX(nhg_ind) AS NHG_IND
					
			  FROM DM_STER.dim_hyp_aanvraagdeel dal
			  
			  WHERE dal.dm_eind_dt = '9999-12-31'
			  GROUP BY 1
			) aanv_deel 
	   ON  aanv_deel.aanvraag_nr = da.aanvraag_nr
       
	   LEFT OUTER JOIN (
	  				    SELECT hav.dossier_nr
					   		, hbs.GEBRUIKER_ID
							, hbs.offerte_oid
							, hbs.begin_dt
							, hbs.BEWIJSSTUK_INSTANCEID
							, hbs.BEWIJSSTUK_NR
							, hbs.BEWIJSSTUK_TYPE_TKT
							, hbs.BEWIJSSTUK_CATEGORIE_CD
							, hbs.ONTVANGEN_IND
							, hbs.ONTVANGST_DT
							, hbs.AKKOORD_IND
							, hbs.AKKOORD_DT 							
							, hbs.NIET_AKKOORD_IND							
							, hbs.NIET_AKKOORD_DT
							, hbs.NIET_NODIG_IND
							, hbs.NIET_NODIG_DT
							, hbs.REDEN_NIET_AKKOORD_TKT
							, hbs.REDEN_NIET_NODIG_TKT
							, hbs.HANDMATIG_TOEGEVOEGD_IND
							, hbs.AUTOMATISCH_TOEVOEGEN_IND
							, hbs.INTERNE_STATUS_CD_TKT
							, hbs.BEWIJSSTUK_STATUS_CD
							, hbs.BEWIJSSTUK_OMSCHRIJVING_TKT
							, hbs.VOLG_NR
							, hbs.WIJZIGING_DT
              
			  		   FROM DWH.hys_aanvraagversie  hav           

              		   JOIN DWH.hys_bewijsstuk      hbs           
              	       ON hav.offerte_oid = hbs.offerte_oid
              
              		   WHERE hbs.GELDIG_IND = 1
              		   AND hbs.EIND_DT = '9999-12-31'
              		   AND hav.EIND_DT = '9999-12-31'
              		   AND hav.GELDIG_IND = 1
              		   AND hav.dossier_nr >= 188700
              		   
              		   QUALIFY hbs.VOLG_NR = MIN (hbs.VOLG_NR) OVER ( 
					   												PARTITION BY hav.dossier_nr, hbs.BEWIJSSTUK_INSTANCEID, hbs.BEWIJSSTUK_NR
                                        							)   --deze toegevoegd om de eerste status/situatie van een stuk op te vragen
            		   ) hbs           
       ON hbs.dossier_nr = da.dossier_nr

 	WHERE da.invoer_aanvraag_dt >='2019-01-01'   
	AND hbs.WIJZIGING_DT > da.offerte_dt
	   )

SELECT kalender_datum
, bank_naam
, bedieningsconcept
, offerte_oid
, aanvraag_nr
, invoer_aanvraag_dt
, wijziging_dt
, aanvraag_status
, INTERNE_STATUS_CD_TKT
, BEWIJSSTUK_STATUS_CD
, BEWIJSSTUK_INSTANCEID
, BEWIJSSTUK_NR
, BEWIJSSTUK_CATEGORIE_CD
, BEWIJSSTUK_TYPE_TKT
, BEWIJSSTUK_OMSCHRIJVING_TKT
, omzetting_ind
, zakelijk_inkomen_ind
, nhg_ind
, betreft_maatwerk_tkt

FROM CTE_FTR;